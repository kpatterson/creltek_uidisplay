#include "UITextDisplay.h"
// ---------------------------------------------------------------------------
// Copyright (C) 2015 Kevin H. Patterson
// Created on 2015-12-28.
//
// @license
// Creltek::UITextDisplay by Kevin H. Patterson is licensed under a Creative
// Commons Attribution-ShareAlike 4.0 International License.
// Based on a work at https://bitbucket.org/kpatterson/creltek_uitextdisplay
//
// This software is furnished "as is", without technical support, and with no
// warranty, express or implied, as to its usefulness for any purpose.
//
// Thread Safe: No
// Extendable: Yes
//
// @file UITextDisplay.cpp
// These classes allow you to implement text-based "display items" with menu
// selection capabilites and lazy drawing. Designed for use with character-
// based LCD displays and the "new_liquidcrystal" library or similar.
//
// @dependencies
// Standard Arduino libraries
// "new_liquidcrystal" library:
//   https://bitbucket.org/fmalpartida/new-liquidcrystal/wiki/Home
//
// @author Kevin H. Patterson - kevpatt _at_ khptech _dot_ com
// ---------------------------------------------------------------------------

namespace Creltek {

	void DisplayUpdateZone::Dirty( char* i_CharPtr, uint8_t i_Length ) {
		if( !CharPtr ) {
			CharPtr = i_CharPtr;
			Length = i_Length;
		} else {
			char* OldEnd = CharPtr + Length;
			char* End = i_CharPtr + i_Length;
			if( OldEnd > End )
				End = OldEnd;

			CharPtr = i_CharPtr;
			Length = End - CharPtr;
		}
	}


	void DisplayItem::PrintText( const char* i_Text, uint8_t i_Pos ) {
		if( !m_Chars )
			return;

		uint8_t i = 0;
		for( uint8_t c = i_Pos; c < m_Length; ++c ) {
			if( i_Text[i] == 0 )
				break;

			m_Chars[c] = i_Text[i];
			++i;
		}
		if( i )
			m_UpdateZone.Dirty( &m_Chars[i_Pos], i );
	}


	void DisplayItem::PrintFloatR( float i_Value, uint8_t i_FracDigits ) {
		if( !m_Chars )
			return;

		float m = 1.0;
		for( uint8_t i = 0; i < i_FracDigits; ++i )
			m *= 10.0;

		uint32_t l = (abs( i_Value ) + 0.5 / m) * m;

		uint8_t c = m_Length - 1;
		for( uint8_t i = 0; c > 0 && i < i_FracDigits; ++i ) {
			m_Chars[c] = 48 + l % 10;
			l /= 10;
			--c;
		}
		if( c > 0 ) {
			m_Chars[c] = '.';
			--c;
		}
		p_PrintIntR( i_Value < 0.0, l, c );

		m_UpdateZone.Dirty( &m_Chars[1], m_Length - 1 );
	}


	void DisplayItem::p_PrintIntR( bool i_Neg, uint32_t& l, uint8_t& c ) {
		if( c > 0 ) {
			m_Chars[c] = 48 + (l % 10);
			l /= 10;
			--c;
			while( c > 0 && l ) {
				m_Chars[c] = 48 + (l % 10);
				l /= 10;
				--c;
			}
			if( c > 0 && i_Neg ) {
				m_Chars[c] = '-';
				--c;
			}
			while( c > 0 ) {
				m_Chars[c] = ' ';
				--c;
			}
		}
	}


	void UITextDisplay::Update( DisplayUpdateZone& i_UpdateZone ) {
		if( !i_UpdateZone )
			return;

		const int Offset = i_UpdateZone.CharPtr - m_Chars;
		const char c = i_UpdateZone.CharPtr[i_UpdateZone.Length];
		i_UpdateZone.CharPtr[i_UpdateZone.Length] = 0;  // make sure string is null-terminated

		m_Chars[32] = 0;
		m_LCD.setCursor( Offset % 16, Offset / 16 );
		m_LCD.print( i_UpdateZone.CharPtr );

		i_UpdateZone.CharPtr[i_UpdateZone.Length] = c;

		i_UpdateZone.Clear();
	}


	void UITextDisplay::UpdateAll() {
		if( !m_RootItem )
			return;

		DisplayItem* CurrentItem = m_RootItem;
		const DisplayItem* FirstItem = CurrentItem;

		Update( *CurrentItem );
		CurrentItem = CurrentItem->m_NextItem;

		while( CurrentItem && CurrentItem != FirstItem ) {
			Update( *CurrentItem );
			CurrentItem = CurrentItem->m_NextItem;
		}
	}


	void UITextDisplay::OnSteps( int8_t i_Steps ) {
		if( !m_ActiveItem ) {
			if( !m_RootItem )
				return;

			m_ActiveItem = m_RootItem;
		}

		if( m_Entered ) {
			DisplayUpdateZone u = m_ActiveItem->OnSteps( i_Steps );
			if( u )
				Update( u );

		} else {
			DisplayUpdateZone u = m_ActiveItem->OnDeselect();
			if( u )
				Update( u );

			m_StepCounter += i_Steps;
			if( m_StepCounter > 2 ) {
				m_ActiveItem = m_ActiveItem->m_NextItem;
				m_StepCounter = 0;
			}
			else if( m_StepCounter < -2 ) {
				m_ActiveItem = m_ActiveItem->m_PrevItem;
				m_StepCounter = 0;
			}

			u = m_ActiveItem->OnSelect();
			if( u )
				Update( u );
		}
	}


	void UITextDisplay::OnSelect() {
		if( !m_ActiveItem )
			return;

		DisplayUpdateZone u;
		m_Entered = !m_Entered;
		if( m_Entered )
			u = m_ActiveItem->OnEnter();
		else
			u = m_ActiveItem->OnExit();

		if( u )
			Update( u );
	}


	bool UITextDisplay::AddItem( DisplayItem* i_DisplayItem, uint8_t i_Row, uint8_t i_Col, DisplayItem* i_AfterItem ) {
		if( !i_DisplayItem )
			return false;
		if( i_Row >= 2 || i_Col >= 16 )
			return false;

		i_DisplayItem->m_Chars = &m_Chars[i_Row * 16 + i_Col];
		
		if( i_AfterItem ) {
			i_DisplayItem->m_PrevItem = i_AfterItem;
			i_DisplayItem->m_NextItem = i_AfterItem->m_NextItem;
			i_DisplayItem->m_PrevItem->m_NextItem = i_DisplayItem;
			i_DisplayItem->m_NextItem->m_PrevItem = i_DisplayItem;
			return true;
			
		} else {
			i_DisplayItem->m_PrevItem = i_DisplayItem;
			i_DisplayItem->m_NextItem = i_DisplayItem;
			m_RootItem = i_DisplayItem;
			return true;
		}
		
		return false;
	}

}
