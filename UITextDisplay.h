#ifndef _Creltek_UITextDisplay_h_
#define _Creltek_UITextDisplay_h_
// ---------------------------------------------------------------------------
// Copyright (C) 2015 Kevin H. Patterson
// Created on 2015-12-28.
//
// @license
// Creltek::UITextDisplay by Kevin H. Patterson is licensed under a Creative
// Commons Attribution-ShareAlike 4.0 International License.
// Based on a work at https://bitbucket.org/kpatterson/creltek_uitextdisplay
//
// This software is furnished "as is", without technical support, and with no
// warranty, express or implied, as to its usefulness for any purpose.
//
// Thread Safe: No
// Extendable: Yes
//
// @file UITextDisplay.h
// These classes allow you to implement text-based "display items" with menu
// selection capabilites and lazy drawing. Designed for use with character-
// based LCD displays and the "new_liquidcrystal" library or similar.
//
// @dependencies
// Standard Arduino libraries
// "new_liquidcrystal" library:
//   https://bitbucket.org/fmalpartida/new-liquidcrystal/wiki/Home
//
// @author Kevin H. Patterson - kevpatt _at_ khptech _dot_ com
// ---------------------------------------------------------------------------

#include <LCD.h>

namespace Creltek {

	struct DisplayUpdateZone {
		DisplayUpdateZone()
		: CharPtr( 0 )
		, Length( 0 )
		{}

		DisplayUpdateZone( char* i_CharPtr, uint8_t i_Length )
		: CharPtr( i_CharPtr )
		, Length( i_Length )
		{}

		operator bool() const {
			return CharPtr;
		}

		void Dirty( char* i_CharPtr, uint8_t i_Length );

		void Clear() {
			CharPtr = 0;
			Length = 0;
		}

		char* CharPtr;
		uint8_t Length;
	};


	class DisplayItem {
	public:
		DisplayItem( uint8_t i_Length )
		: m_Chars( 0 )
		, m_PrevItem( 0 )
		, m_NextItem( 0 )
		, m_Length( i_Length )
		{}

		virtual DisplayUpdateZone& OnSteps( int8_t i_Steps ) {
			return m_UpdateZone;
		}

		virtual DisplayUpdateZone& OnSelect() {
			if( m_Chars ) {
				m_Chars[0] = '>';
				m_UpdateZone.Dirty( &m_Chars[0], 1 );
			}
			return m_UpdateZone;
		}

		virtual DisplayUpdateZone& OnDeselect() {
			if( m_Chars ) {
				m_Chars[0] = ' ';
				m_UpdateZone.Dirty( &m_Chars[0], 1 );
			}
			return m_UpdateZone;
		}

		virtual DisplayUpdateZone& OnEnter() {
			if( m_Chars ) {
				m_Chars[0] = '*';
				m_UpdateZone.Dirty( &m_Chars[0], 1 );
			}
			return m_UpdateZone;
		}

		virtual DisplayUpdateZone& OnExit() {
			if( m_Chars ) {
				m_Chars[0] = '>';
				m_UpdateZone.Dirty( &m_Chars[0], 1 );
			}
			return m_UpdateZone;
		}

		void PrintText( const char* i_Text, uint8_t i_Pos = 0 );

		void PrintIntR( int32_t i_Value ) {
			if( !m_Chars )
				return;

			uint32_t l = abs( i_Value );
			uint8_t c = m_Length - 1;
			p_PrintIntR( i_Value < 0, l, c );
			m_UpdateZone.Dirty( &m_Chars[1], m_Length - 1 );
		}

		void PrintFloatR( float i_Value, uint8_t i_FracDigits = 2 );

		DisplayItem* GetPrevItem() { return m_PrevItem; }
		DisplayItem* GetNextItem() { return m_NextItem; }

	private:
		void p_PrintIntR( bool i_Neg, uint32_t& l, uint8_t& c );

	protected:
		DisplayUpdateZone m_UpdateZone;

	private:
		friend class UITextDisplay;

		char* m_Chars;
		DisplayItem* m_PrevItem;
		DisplayItem* m_NextItem;
		uint8_t m_Length;
	};


	class UITextDisplay {
	public:
		UITextDisplay( LCD& i_LCD )
		: m_LCD( i_LCD )
		, m_RootItem( 0 )
		, m_ActiveItem( 0 )
		, m_Entered( false )
		, m_StepCounter( 0 )
		{
			for( int i = 0; i < 33; ++i )
				m_Chars[i] = 0;
		}

		bool init() {
//			m_LCD.begin( 16, 2 );
			return true;
		}

		void Clear() {
			for( int i = 0; i < 32; ++i )
				m_Chars[i] = ' ';
			m_LCD.clear();
		}

		void Refresh() {
			m_Chars[32] = 0;
			m_LCD.setCursor( 0, 0 );
			const char t = m_Chars[16];
			m_Chars[16] = 0;
			m_LCD.print( &m_Chars[0] );
			m_LCD.setCursor( 0, 1 );
			m_Chars[16] = t;
			m_LCD.print( &m_Chars[16] );
		}

		LCD& GetLCD() { return m_LCD; }

		void Update( DisplayUpdateZone& i_UpdateZone );

		void Update( DisplayItem& i_Item ) {
			Update( i_Item.m_UpdateZone );
		}

		void UpdateAll();
		
		void OnSteps( int8_t i_Steps );
		
		void OnSelect();
		
		bool AddItem( DisplayItem* i_DisplayItem, uint8_t i_Row, uint8_t i_Col, DisplayItem* i_AfterItem );
		
	private:
		LCD& m_LCD;
		DisplayItem* m_RootItem;
		DisplayItem* m_ActiveItem;
		char m_Chars[2 * 16 + 1];
		bool m_Entered;
		int8_t m_StepCounter;
	};

}

#endif // _Creltek_UITextDisplay_h_
